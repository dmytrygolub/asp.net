﻿using Furniture_Shop.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Furniture_Shop.Controllers
{
    public class FurnituresController : Controller
    {
        private readonly IAllFurniture _allFurniture;
        private readonly IFurnitureCategory _furnitureCategory;

        public FurnituresController(IAllFurniture allFurniture, IFurnitureCategory furnitureCategory)
        {
            _allFurniture = allFurniture;
            _furnitureCategory = furnitureCategory;
        }

        public ViewResult List()
        {
            var furnitures = _allFurniture.Furnitures;
            return View(furnitures);
        }
    }
}